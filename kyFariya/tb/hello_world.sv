module hello_world;

    `include "tb_essentials.sv"

    initial begin
        $display("HELLO WORLD!");
    end

endmodule